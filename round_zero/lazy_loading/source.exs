defmodule LazyLoading do
  @input_file     "lazy_loading.txt"
  @output_file    "lazy_loading_output.txt"
  @minimum_weight 50
  
  @moduledoc """
  **Problem 2 [Round 0]**: Lazy Loading
  
  Refer to: https://www.facebook.com/hackercup/problem/169401886867367/
  """

  def start() do
    result_data = File.read!(@input_file)
      |> parse_data
      |> Stream.map(&Enum.sort/1)
      |> Stream.map(&compute_trips/1)
      |> Stream.with_index
      |> Stream.map(&format_case/1)
      |> Enum.join("\n")

    File.write!(@output_file, result_data <> "\n")
  end
  
  defp parse_data(input_data) do
    String.split(input_data, "\n", trim: true)
    |> List.delete_at(0)
    |> Enum.map(&parse_int/1)
    |> Enum.reduce({[], 0, []}, &day_info/2)
    |> elem(0)
  end
  
  defp format_case({trips, index}) do
    "Case ##{index + 1}: #{trips}"
  end
  
  defp day_info(elem, state) do
    case state do
      {days, 0   , _  } -> {days, elem, []}
      {days, 1   , bag} -> {days ++ [bag ++ [elem]], 0, []}
      {days, left, bag} -> {days, left - 1, bag ++ [elem]}
    end
  end
  
  # Core Algorithm
  defp compute_trips(items, trips \\ 0)
  defp compute_trips([], trips), do: trips
  defp compute_trips(items, trips) do
    heaviest  = List.last(items)
    lighter   = List.delete_at(items, -1)
    min_items = min_factor(heaviest, @minimum_weight)

    cond do
      length(items) >= min_items -> lighter
        |> slice_from(min_items - 1)
        |> compute_trips(trips + 1)
      :otherwise -> trips
    end
  end
  
  defp min_factor(n, t, f \\ 1) do
    if (n * f >= t), do: f, else: min_factor(n, t, f + 1)
  end
  
  defp slice_from(list, index), do: list
    |> Enum.slice(index, length(list))
  
  defp parse_int(data_str), do: Integer.parse(data_str)
    |> elem(0)
end

LazyLoading.start()