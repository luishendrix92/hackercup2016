defmodule ProgressPie do
  @input_file  "progress_pie.txt"
  @output_file "progress_pie_output.txt"
  @radius      50 # Pixels of radius
  
  @moduledoc """
  **Problem 1 [Round 0]**: Progress Pie
  
  Refer to: https://www.facebook.com/hackercup/problem/1254819954559001/
  """
  
  def start() do
    {:ok, input}  = File.open(@input_file, [:read])
    {:ok, output} = File.open(@output_file, [:append])

    IO.read(input, :line) # Discard first line  
    write_output(input, output, 1)
    
    File.close(input)
    File.close(output)
  end
  
  defp write_output(input, output, case_num) do
    case IO.read(input, :line) do
      :eof -> :finished
      line ->
        IO.write(output, data_to_case(line, case_num))
        write_output(input, output, case_num + 1)        
    end
  end
  
  # Turns a read line from input (formatted as "A B C\n" where A
  # is the progress and {B, C} the given point) into to a string
  # to be written into the output. [String -> String].
  defp data_to_case(line, case_num) do
    prog_and_coords = Regex.split(~r{\s}, line, trim: true)
    case_color      = prog_and_coords    
      |> Enum.map(&parse_float/1)
      |> List.to_tuple
      |> point_color
      
    "Case ##{case_num}: " <> case_color <> "\n"
  end
  
  # Takes a coordinate tuple and returns the vector magnitude
  # using the Pythagorean Theorem (hypotenuse from origin
  # to the given point). [Tuple -> Float].
  defp vector_dist({x, y}), do: :math.sqrt(x * x + y * y)
  
  # Through a coordinate tuple, determines angle in degrees
  # formed by the line that connects the origin the point
  # starting from the positive Y axis [Tuple -> Float].
  defp vector_angle({x, y}) do
    angle = :math.atan2(x, y) * (180 / :math.pi)
    
    if angle < 0, do: angle + 360, else: angle
  end
  
  # As instructed, a progress percentage (not in decimal) gets
  # transformed into an angle from 0° to 360°. [Float -> Float]
  defp prog_angle(prog), do: (prog / 100) * 360
  
  # Receives a tuple with three elements (progress and a point)
  # and returns a string that says if the point is black or
  # white given the rule that in order for a point to be black,
  # it needs to be inside the circle (radius of 50). It's in
  # the circle if the angle is between 0 and the angle of the
  # progress and also if the magnitude is between 0 and
  # the radius of the circle. [Tuple -> String]
  defp point_color({progress, x, y}) do
    point       = {x - @radius, y - @radius}
    in_progress = vector_angle(point) <= prog_angle(progress)
    in_radius   = vector_dist(point)  <= @radius
    
    if in_progress and in_radius, do: "black", else: "white"
  end
  
  # Parses a string "5.4" into a number. [String -> Float]
  defp parse_float(data_str) do
    Float.parse(data_str) |> elem(0)
  end
end

ProgressPie.start()