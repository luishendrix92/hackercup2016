const combinatory = (n, r) => factorial(n) / (factorial(r) * factorial(n - r))
const factorial = (n) => {
  return !n? 1 : n * factorial(n - 1)
}

Array.prototype.last = function() {
  return this[this.length - 1]
}

const probability = (rolls, sides, damage) => [
  ...Array(Math.floor((damage - rolls) / sides) + 1)
].reduce((ways, _, r) => {
  if (damage === 172) {
    console.log(damage, r, ways)
  }
  
  return ways + (
    combinatory(
      damage - (sides * r) - 1,
      rolls - 1
    ) *
    combinatory(rolls, r) *
    Math.pow(-1, r)
  )
}, 0) / Math.pow(sides, rolls)
/*
def spell_chance(min_damage, {rolls, sides, extra}) do
  chance = rolls..(sides * rolls)
    |> Stream.filter(fn damage -> (damage + extra) >= min_damage end)
    |> Stream.map(&(damage_chance(sides, rolls, &1)))
    |> Enum.sum
  
  if chance == 0, do: 0, else: Float.round(chance, 6)
end
*/
function spellChance(minDamage, [rolls, sides, extra]) {
  let chance = [...Array((sides * rolls) - rolls + 1)]
    .map((_, i) => i + rolls)
    .filter(damage => damage + extra >= minDamage)
    .map(damage => probability(rolls, sides, damage))
    
  return chance
}

console.log(
  spellChance(722, [16, 20, 629])
)