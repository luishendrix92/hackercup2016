defmodule Algorithm do
  # best_spell? :: Zombie Info -> Probability
  # best_spell? :: Tuple/3 {Int, List} -> Float
  def best_spell?({min_damage, spells}), do: spells
    |> Stream.filter(fn {rolls, sides, extra} ->
      not (min_damage > ((sides * rolls) + extra))
    end)
    |> Stream.map((&spell_chance(min_damage, &1)))
    |> Enum.max
  
  # spell_chance :: Minimum damage -> Spell -> Probability
  # spell_chance :: Int -> Tuple/3 -> Float
  def spell_chance(min_damage, {rolls, sides, extra}) do
    chance = rolls..(sides * rolls)
      |> Stream.filter(fn damage -> (damage + extra) >= min_damage end)
      |> Stream.map(&(damage_chance(sides, rolls, &1)))
      |> Enum.sum
    
    if chance == 0, do: 0, else: Float.round(chance, 6)
  end
  
  # Refer to: http://mathworld.wolfram.com/Dice.html
  # damage_chance :: Sides -> Rolls -> Damage -> Probability
  # damage_chance :: Int -> Int -> Int -> Float
  def damage_chance(sides, rolls, damage) do
    ways = 0..(floor((damage - rolls) / sides))
      |> Enum.reduce(0, fn (r, ways) ->  ways + (
          combinatory(damage - (sides * r) - 1, rolls - 1)
          * combinatory(rolls, r)
          * :math.pow(-1, r))
      end)
      
    ways / :math.pow(sides, rolls)
  end  
  
  # combinatory :: Int -> Int -> Int
  def combinatory(n, r) do
    (factorial(n) / (factorial(r) * factorial(n - r)))
    |> round
  end
  
  # factorial :: Int -> Int
  def factorial(n, acc \\ 1)
  def factorial(0, acc), do: acc
  def factorial(n, acc) do
    factorial(n - 1, n * acc)
  end
  
  # floor :: Float -> Integer
  def floor(n), do: round(Float.floor(n))
end

defmodule FightingZombie do
  import Algorithm
  
  @input_file  "fighting_the_zombie.txt"
  @output_file "fighting_the_zombie_output.txt"
  
  @moduledoc """
  **Problem 3 [Round 0]**: Fighting the Zombie
  
  Refer to: https://www.facebook.com/hackercup/problem/326053454264498/
  """
  
  def start() do
    output_data = File.read!(@input_file)
      |> parse_zombies
      |> Stream.map(&best_spell?/1)
      |> Stream.with_index
      |> Stream.map(&format_case/1)
      |> Enum.join("\n")

    File.write!(@output_file, output_data <> "\n")
  end
  
  # parse_zombies :: File Data -> List of zombie data
  # parse_zombies :: String ->
  #   List (:: Tuple {Int, List (:: Tuple/3 {Int, Int, Int})})
  def parse_zombies(data), do: data
    |> String.split("\n", trim: true)
    |> List.delete_at(0)
    |> Stream.chunk(2)
    |> Enum.map(fn [info, spells] ->
      [parse_damage(info), parse_spells(spells)]
      |> List.to_tuple
    end)
  
  # parse_damage :: Zombie First line -> Minimum Damage
  # parse_damage :: String -> Int
  def parse_damage(zombie_info), do: zombie_info
    |> String.split(" ", trim: true)
    |> Enum.map(&parse_int/1)
    |> List.first

  # parse_spells :: Spells -> List {rolls, sides, extra}
  # parse_spells :: String -> List (::Tuple/3 {Int, Int, Int})
  def parse_spells(spells) do
    pattern = ~r{(\d+)d(\d+)([\-\+]\d+)?}
    
    String.split(spells, " ", trim: true)
    |> Enum.map(fn spell ->
      Regex.run(pattern, spell, capture: :all_but_first)
      |> Enum.map(&parse_int/1)
      |> add_extra
      |> List.to_tuple
    end)
  end
  
  # format_case :: Case -> Case #N: Probab.
  # format_case :: Tuple/2 {Float, Int} -> String
  def format_case({probability, index}) do
    prob_str = probability
      |> to_string
      |> String.ljust(8, ?0)
      
    "Case ##{index}: #{prob_str}"
  end

  def parse_int(str), do: str
    |> Integer.parse
    |> elem(0)

  def add_extra(info) do
    if length(info) < 3, do: info ++ [0], else: info
  end
end

(
  Algorithm.factorial(171)
)
|> IO.puts